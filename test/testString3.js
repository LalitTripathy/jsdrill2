// Given a string in the format of "10/1/2021", print the month in which the date is present in.
let month ={
    1:"Jan",
    2:"Feb",
    3:"Mar",
    4:"apr",
    5:"May",
    6:"June",
    7:"Jul",
    8:"Aug",
    9:"Sept",
    10:"Oct",
    11:"Nov",
    12:"Dec"
};
let prb3 = require("../string3");
let ans = prb3("10/1/2021");
let m = String(ans);
// console.log(ans);
console.log(month[m]);

