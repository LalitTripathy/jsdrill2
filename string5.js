// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.
// const str_prb5 = ["the", "quick", "brown", "fox"];

function displayProblem5(str1)
{
    //const str_prb5 = ["the", "quick", "brown", "fox"];

    str1.toString();
    console.log(str1);
    let sentence = str1.join(" ");
    return sentence;
    //console.log(sentence);
    
}



module.exports = displayProblem5;

